﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace wfRpa
{
    public partial class Form1 : Form
    {
 

        public Form1()
        {
            InitializeComponent();
            richTextBox1.Visible = false;
            labelResultado.Visible = false;

            dataGridView1.Visible = false;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void GerarPessoa_Click(object sender, EventArgs e)
        {

            //if (comboBox1.Text == string.Empty)
            //{
            //    MessageBox.Show("Selecione um tipo de sexo");
            //    return;
            //}


            if(numericUpDown1.Value == 0 || numericUpDown1.Value < 0)
            {
                MessageBox.Show("a idade invalida");
                return;
            }
            if (numericUpDown1.Value <= 17)
            {
                MessageBox.Show("pessoa dever ser maior de idade");
                return;
            }

         

            try
            {
                //Abrir navegor Chrome
                IWebDriver driver = new ChromeDriver();

                try
                {



                    //Acessa central vip de valmiki
                    driver.Navigate().GoToUrl("https://www.4devs.com.br/");

                    //Maximiza tela
                    driver.Manage().Window.Maximize();

                    //clica no nav do item de gerar pessoa
                    // driver.FindElement(By.XPath("/html/body/main/div/div[1]/div/nav/div/ul/li[22]/a"));
                    driver.FindElement(By.XPath("/html/body/main/div/div[1]/div/nav/div/ul/li[22]/a")).Click();

                    //Seleciona o tipo do sexo
                    switch (comboBox1.Text)
                    {
                        case "Masculino":
                            driver.FindElement(By.Id("sexo_homem")).Click();
                            break;
                        case "Feminino":
                            driver.FindElement(By.Id("sexo_mulher")).Click();
                            break;
                        default:
                            driver.FindElement(By.Id("sexo_indiferente")).Click();
                            break;
                    }




                    //Clica na dropdown de ordenação
                    SelectElement oSelectOrdem = new SelectElement(driver.FindElement(By.Id("idade")));

                    //Seleciona o opção de ascedente para ordenar
                    oSelectOrdem.SelectByText(numericUpDown1.Value.ToString());


                    //Clica no botao 
                    driver.FindElement(By.Id("bt_gerar_pessoa")).Click();

                    Thread.Sleep(5000);

                    //Clica no json
                    driver.FindElement(By.Id("btn_json_tab")).Submit();

                    var json = driver.FindElement(By.Id("dados_json")).Text;


                    //passa valores recuperado para uma textearea
                    richTextBox1.Text = json;

                    richTextBox1.Visible = true;

                    labelResultado.Visible = true;
                    
                    //Clica no fom para exibir os dados gerados
                    driver.FindElement(By.Id("btn_form_tab")).Click();

                    //Recupera do input do nome
                    var nomeCliente = driver.FindElement(By.Id("nome")).FindElement(By.TagName("span")).Text;

                    //Recupera do input do cpf
                    var cpf = driver.FindElement(By.Id("cpf")).FindElement(By.TagName("span")).Text;

                    //Recupera do input do email
                    var email = driver.FindElement(By.Id("email")).FindElement(By.TagName("span")).Text;

                    dataGridView1.Rows.Add();

                    dataGridView1.Rows[0].Cells[0].Value = nomeCliente;
                    dataGridView1.Rows[0].Cells[1].Value = cpf;
                    dataGridView1.Rows[0].Cells[2].Value = email;
                    dataGridView1.Visible = true;

                }
                finally
                {
                    driver.Quit();
                    MessageBox.Show("Processo finalizado");
                }
                

            }
            catch (Exception )
            {


                MessageBox.Show("Ocorreu um erro no robo ");
            }




































        }
    }
}
